package cn.zynworld.wwjcloud.wwjcloud.wxsdk;

import org.junit.Test;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @auther Buynow Zhao
 * @create 2018/5/29
 * 定时任务测试
 */
public class TimerTest {
	private final Scanner sc = new Scanner(System.in);


	@Test
	public void test1() throws Exception {
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				System.out.println("-------设定要指定任务--------");
			}
		}, 2000);// 设定指定的时间time,此处为2000毫秒
		sc.nextLine();
	}

	@Test
	public void test2() throws Exception {
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				System.out.println("-------设定要指定任务--------");
			}
		}, 1000, 3000);
		sc.nextLine();
	}

	@Test
	public void test3() throws Exception {
		Integer i = 1382694957;
		System.out.println(i);
		i = (int) System.currentTimeMillis();
		System.out.println(i);
	}
}
