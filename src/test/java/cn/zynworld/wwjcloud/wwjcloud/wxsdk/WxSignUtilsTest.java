package cn.zynworld.wwjcloud.wwjcloud.wxsdk;

import cn.zynworld.wwjcloud.wwjcloud.wxsdk.utlis.WxSignUtil;
import org.junit.Test;

/**
 * @auther Buynow Zhao
 * @create 2018/6/1
 */
public class WxSignUtilsTest {

	@Test
	public void testCreateJsSign() throws Exception {
		String sign = WxSignUtil.createJsApiSignature("HoagFKDcsGMVCIY2vOjf9guwWLfZGD5hwOpq_MQ_J_YJ4hdaCLZJKIupAJaSgnx2Wja8TyuXanqWG4fGcnSWgg",
				"http://wxwebdemo.ngrok.zynworld.cn"
				, "250a330c-4b84-426a-b748-f6f4801363ed",
				1527782726L);
		// FB677D412B0E7C70EBDB8326966D394FD170D748
		System.out.println(sign);
	}
}
