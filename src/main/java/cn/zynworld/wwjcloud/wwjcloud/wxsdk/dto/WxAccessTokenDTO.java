package cn.zynworld.wwjcloud.wwjcloud.wxsdk.dto;

/**
 * @auther Buynow Zhao
 * @create 2018/5/27
 */
public class WxAccessTokenDTO {
	private String access_token;
	private Integer expires_in;

	public String getAccess_token() {
		return access_token;
	}

	public WxAccessTokenDTO setAccess_token(String access_token) {
		this.access_token = access_token;
		return this;
	}

	public Integer getExpires_in() {
		return expires_in;
	}

	public WxAccessTokenDTO setExpires_in(Integer expires_in) {
		this.expires_in = expires_in;
		return this;
	}

	@Override
	public String toString() {
		return "WxAccessTokenDTO{" +
				"access_token='" + access_token + '\'' +
				", expires_in=" + expires_in +
				'}';
	}
}
