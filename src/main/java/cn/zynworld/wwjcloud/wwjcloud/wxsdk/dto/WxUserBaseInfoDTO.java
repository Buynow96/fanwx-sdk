package cn.zynworld.wwjcloud.wwjcloud.wxsdk.dto;

import java.util.List;

/**
 * @auther Buynow Zhao
 * @create 2018/5/28
 */
public class WxUserBaseInfoDTO {

	// 订阅为1 未订阅为0
	private Integer subscribe;
	private String openid;
	// 用户昵称
	private String nickname;
	// 用户性别 1 男性 2 女性
	private Integer sex;
	private String language;
	private String city;
	private String province;
	private String country;
	private String headimgurl;
	private Long subscribe_time;
	private String unionid;
	private String remark;
	private Integer groupid;
	// 用户被打上的标签ID列表
	private List<Integer> tagid_list;
	// 返回用户关注的渠道来源
	private String subscribe_scene;
	// 二维码扫码场景
	private Integer qr_scene;
	// 二维码扫码场景描述
	private String qr_scene_str;

	public Integer getSubscribe() {
		return subscribe;
	}

	public WxUserBaseInfoDTO setSubscribe(Integer subscribe) {
		this.subscribe = subscribe;
		return this;
	}

	public String getOpenid() {
		return openid;
	}

	public WxUserBaseInfoDTO setOpenid(String openid) {
		this.openid = openid;
		return this;
	}

	public String getNickname() {
		return nickname;
	}

	public WxUserBaseInfoDTO setNickname(String nickname) {
		this.nickname = nickname;
		return this;
	}

	public Integer getSex() {
		return sex;
	}

	public WxUserBaseInfoDTO setSex(Integer sex) {
		this.sex = sex;
		return this;
	}

	public String getLanguage() {
		return language;
	}

	public WxUserBaseInfoDTO setLanguage(String language) {
		this.language = language;
		return this;
	}

	public String getCity() {
		return city;
	}

	public WxUserBaseInfoDTO setCity(String city) {
		this.city = city;
		return this;
	}

	public String getProvince() {
		return province;
	}

	public WxUserBaseInfoDTO setProvince(String province) {
		this.province = province;
		return this;
	}

	public String getCountry() {
		return country;
	}

	public WxUserBaseInfoDTO setCountry(String country) {
		this.country = country;
		return this;
	}

	public String getHeadimgurl() {
		return headimgurl;
	}

	public WxUserBaseInfoDTO setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
		return this;
	}

	public Long getSubscribe_time() {
		return subscribe_time;
	}

	public WxUserBaseInfoDTO setSubscribe_time(Long subscribe_time) {
		this.subscribe_time = subscribe_time;
		return this;
	}

	public String getUnionid() {
		return unionid;
	}

	public WxUserBaseInfoDTO setUnionid(String unionid) {
		this.unionid = unionid;
		return this;
	}

	public String getRemark() {
		return remark;
	}

	public WxUserBaseInfoDTO setRemark(String remark) {
		this.remark = remark;
		return this;
	}

	public Integer getGroupid() {
		return groupid;
	}

	public WxUserBaseInfoDTO setGroupid(Integer groupid) {
		this.groupid = groupid;
		return this;
	}

	public List<Integer> getTagid_list() {
		return tagid_list;
	}

	public WxUserBaseInfoDTO setTagid_list(List<Integer> tagid_list) {
		this.tagid_list = tagid_list;
		return this;
	}

	public String getSubscribe_scene() {
		return subscribe_scene;
	}

	public WxUserBaseInfoDTO setSubscribe_scene(String subscribe_scene) {
		this.subscribe_scene = subscribe_scene;
		return this;
	}

	public Integer getQr_scene() {
		return qr_scene;
	}

	public WxUserBaseInfoDTO setQr_scene(Integer qr_scene) {
		this.qr_scene = qr_scene;
		return this;
	}

	public String getQr_scene_str() {
		return qr_scene_str;
	}

	public WxUserBaseInfoDTO setQr_scene_str(String qr_scene_str) {
		this.qr_scene_str = qr_scene_str;
		return this;
	}


	@Override
	public String toString() {
		return "WxUserBaseInfoDTO{" +
				"subscribe=" + subscribe +
				", openid='" + openid + '\'' +
				", nickname='" + nickname + '\'' +
				", sex=" + sex +
				", language='" + language + '\'' +
				", city='" + city + '\'' +
				", province='" + province + '\'' +
				", country='" + country + '\'' +
				", headimgurl='" + headimgurl + '\'' +
				", subscribe_time=" + subscribe_time +
				", unionid='" + unionid + '\'' +
				", remark='" + remark + '\'' +
				", groupid=" + groupid +
				", tagid_list=" + tagid_list +
				", subscribe_scene='" + subscribe_scene + '\'' +
				", qr_scene=" + qr_scene +
				", qr_scene_str='" + qr_scene_str + '\'' +
				'}';
	}
}
