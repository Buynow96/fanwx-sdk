package cn.zynworld.wwjcloud.wwjcloud.wxsdk.utlis;

import java.util.*;

public class WxSignUtil {

	/**
	 * 验证签名
	 *
	 * @param token     微信服务器token，在env.properties文件中配置的和在开发者中心配置的必须一致
	 * @param signature 微信服务器传过来sha1加密的证书签名
	 * @param timestamp 时间戳
	 * @param nonce     随机数
	 * @return
	 */
	public static boolean checkSignature(String token, String signature, String timestamp, String nonce) {
		String[] arr = new String[]{token, timestamp, nonce};
		// 将token、timestamp、nonce三个参数进行字典序排序
		Arrays.sort(arr);

		// 将三个参数字符串拼接成一个字符串进行sha1加密
		String tmpStr = WxSHA1.encode(arr[0] + arr[1] + arr[2]);

		// 将sha1加密后的字符串可与signature对比，标识该请求来源于微信
		return tmpStr != null ? tmpStr.equals(signature.toUpperCase()) : false;
	}


	/**
	 * 生成 js调用凭证签名
	 *
	 * @param jsapiTicket js调用凭证
	 * @param url         url连接
	 * @param nonceStr    随机字符串
	 * @param timestamp   时间戳
	 * @return js调用凭证签名
	 */
	public static String createJsApiSignature(String jsapiTicket, String url, String nonceStr, Long timestamp) {
		String string1 = "jsapi_ticket=" + jsapiTicket +
				"&noncestr=" + nonceStr +
				"&timestamp=" + timestamp +
				"&url=" + url;
		String signature = WxSHA1.encode(string1);
		return signature.toLowerCase();
	}

	/**
	 * @return 微信时间戳
	 */
	public static Long getTimestamp() {
		return (new Date().getTime()/1000);
	}

	/**
	 * @return 随机字符串
	 */
	public static String getRandomString() {
		// TODO UUID 可能造成性能问题 后续解决
		// 将所有的大小写字母和0-9数字存入字符串中
		return UUID.randomUUID().toString();
	}


}
