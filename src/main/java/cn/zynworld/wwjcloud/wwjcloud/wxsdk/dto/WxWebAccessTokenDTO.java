package cn.zynworld.wwjcloud.wwjcloud.wxsdk.dto;

/**
 * @auther Buynow Zhao
 * @create 2018/5/29
 * 微信公众号网页接口授权的 accessToken
 */
public class WxWebAccessTokenDTO {

	private String access_token;
	private Integer expires_in;
	private String refresh_token;
	private String openid;
	private String scope;

	public String getAccess_token() {
		return access_token;
	}

	public WxWebAccessTokenDTO setAccess_token(String access_token) {
		this.access_token = access_token;
		return this;
	}

	public Integer getExpires_in() {
		return expires_in;
	}

	public WxWebAccessTokenDTO setExpires_in(Integer expires_in) {
		this.expires_in = expires_in;
		return this;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public WxWebAccessTokenDTO setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
		return this;
	}

	public String getOpenid() {
		return openid;
	}

	public WxWebAccessTokenDTO setOpenid(String openid) {
		this.openid = openid;
		return this;
	}

	public String getScope() {
		return scope;
	}

	public WxWebAccessTokenDTO setScope(String scope) {
		this.scope = scope;
		return this;
	}
}
