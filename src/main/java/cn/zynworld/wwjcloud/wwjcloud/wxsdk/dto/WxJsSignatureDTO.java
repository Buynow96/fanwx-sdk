package cn.zynworld.wwjcloud.wwjcloud.wxsdk.dto;

/**
 * @auther Buynow Zhao
 * @create 2018/5/30
 * 微信js api 签名
 */
public class WxJsSignatureDTO {

	private String appId;
	// 时间戳
	private Long timestamp;
	// 随机字符串
	private String nonceStr;
	// 签名
	private String signature;

	public String getAppId() {
		return appId;
	}

	public WxJsSignatureDTO setAppId(String appId) {
		this.appId = appId;
		return this;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public WxJsSignatureDTO setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
		return this;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public WxJsSignatureDTO setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
		return this;
	}

	public String getSignature() {
		return signature;
	}

	public WxJsSignatureDTO setSignature(String signature) {
		this.signature = signature;
		return this;
	}
}
