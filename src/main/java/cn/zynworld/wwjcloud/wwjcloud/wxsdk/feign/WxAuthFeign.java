package cn.zynworld.wwjcloud.wwjcloud.wxsdk.feign;


import cn.zynworld.wwjcloud.wwjcloud.wxsdk.dto.WxAccessTokenDTO;
import cn.zynworld.wwjcloud.wwjcloud.wxsdk.dto.WxJsApiTicketDTO;
import cn.zynworld.wwjcloud.wwjcloud.wxsdk.dto.WxWebAccessTokenDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 认证相关接口
 */
@FeignClient(url = "${wx.sdk.url}",name = "WxBaseSDK")
public interface WxAuthFeign {

	/**
	 * 获得WxAccessToken
	 */
	@GetMapping(path = "/cgi-bin/token")
	public WxAccessTokenDTO getAccessToken(@RequestParam(name = "grant_type",defaultValue = "client_credential") String grantType
			, @RequestParam(name = "appid") String appid, @RequestParam(name = "secret") String secret);


	/**
	 * 用户网页授权 通过授权code 获取 webAccessToken <a href="https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140842">接口详情</a>
	 * @param code 获得的web授权代码
	 * @param grantType 此处填写authorization_code
	 */
	@GetMapping(path = "/sns/oauth2/access_token")
	public WxWebAccessTokenDTO getWebAccessToken(@RequestParam("appid") String appid, @RequestParam("secret") String secret, @RequestParam("code") String code, @RequestParam("grant_type") String grantType);

	/**
	 * 当用户的WxWebAccessToken 失效后可以通过refreshToken来获取新的WebAccessToken
	 */
	@GetMapping(path = "/sns/oauth2/refresh_token")
	public WxWebAccessTokenDTO refreshWebAccessToken(@RequestParam("appid") String appid,@RequestParam("refresh_token") String refreshToken,@RequestParam("grant_type") String grantType);

	/**
	 * 通过accessToken 获取临时js调用凭证
	 */
	@GetMapping(path = "/cgi-bin/ticket/getticket")
	public WxJsApiTicketDTO getWxJsApiTicket(@RequestParam("access_token") String accessToken, @RequestParam("type") String type);
}
