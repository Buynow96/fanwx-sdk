package cn.zynworld.wwjcloud.wwjcloud.wxsdk.sdk;

import cn.zynworld.wwjcloud.wwjcloud.wxsdk.dto.WxUserBaseInfoDTO;
import cn.zynworld.wwjcloud.wwjcloud.wxsdk.feign.WxUserFeign;
import cn.zynworld.wwjcloud.wwjcloud.wxsdk.manager.WxInfoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @auther Buynow Zhao
 * @create 2018/5/30
 * 与用户相关的操作封装
 */
@Component
public class WxUserSDK {

	@Autowired
	private WxUserFeign wxUserFeign;
	@Autowired
	private WxInfoManager wxInfoManager;
	@Autowired
	private WxAuthSDK wxAuthSDK;

	/**
	 * 通过openid获取用户基本信息
	 * @param openid 用户的openid
	 */
	public WxUserBaseInfoDTO getUserInfoByOpenid(String openid) {
		return wxUserFeign.getUserInfoByOpenId(wxAuthSDK.getWxAccessTokenString()
				,openid,"zh_CN");
	}
}
