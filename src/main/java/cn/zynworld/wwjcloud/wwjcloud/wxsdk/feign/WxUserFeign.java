package cn.zynworld.wwjcloud.wwjcloud.wxsdk.feign;

import cn.zynworld.wwjcloud.wwjcloud.wxsdk.dto.WxUserBaseInfoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(url = "${wx.sdk.url}",name = "WxBaseSDK")
public interface WxUserFeign {


	/**
	 * @param accessToken 微信凭证
	 * @param openid 用户openId
	 * @param lang 语言
	 * @return 得到用户基本信息
	 */
	@GetMapping(path = "/cgi-bin/user/info")
	public WxUserBaseInfoDTO getUserInfoByOpenId(@RequestParam("access_token") String accessToken
			,@RequestParam("openid") String openid,@RequestParam("lang") String lang);

}
