package cn.zynworld.wwjcloud.wwjcloud.wxsdk.manager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @auther Buynow Zhao
 * @create 2018/5/29
 * 集中存储微信相关数据
 */
@Component
public class WxInfoManager {
	@Value("${wx.appid}")
	private String appid;
	@Value("${wx.appsecret}")
	private String appsecret;
	//微信调用接口 url
	@Value("${wx.sdk.url}")
	private String sdkUrl;
	@Value("${wx.token}")
	private String token;

	public String getAppid() {
		return appid;
	}

	public String getAppsecret() {
		return appsecret;
	}

	public String getSdkUrl() {
		return sdkUrl;
	}

	public String getToken() {
		return token;
	}
}
