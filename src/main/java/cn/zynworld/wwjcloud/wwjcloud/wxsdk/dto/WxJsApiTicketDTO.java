package cn.zynworld.wwjcloud.wwjcloud.wxsdk.dto;

/**
 * @auther Buynow Zhao
 * @create 2018/5/30
 * 微信公众号调用js sdk的临时凭证
 * 有效期两小时 由WxAuthSDK 维护更新
 */
public class WxJsApiTicketDTO {

	private Integer errcode;
	private String errormsg;
	private String ticket;
	private Integer expires_in;

	public Integer getErrcode() {
		return errcode;
	}

	public WxJsApiTicketDTO setErrcode(Integer errcode) {
		this.errcode = errcode;
		return this;
	}

	public String getErrormsg() {
		return errormsg;
	}

	public WxJsApiTicketDTO setErrormsg(String errormsg) {
		this.errormsg = errormsg;
		return this;
	}

	public String getTicket() {
		return ticket;
	}

	public WxJsApiTicketDTO setTicket(String ticket) {
		this.ticket = ticket;
		return this;
	}

	public Integer getExpires_in() {
		return expires_in;
	}

	public WxJsApiTicketDTO setExpires_in(Integer expires_in) {
		this.expires_in = expires_in;
		return this;
	}
}
