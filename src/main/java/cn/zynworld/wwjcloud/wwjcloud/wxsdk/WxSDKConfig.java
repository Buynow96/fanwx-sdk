package cn.zynworld.wwjcloud.wwjcloud.wxsdk;

import cn.zynworld.wwjcloud.wwjcloud.wxsdk.utlis.WxMessageConverter;
import feign.codec.Decoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @auther Buynow Zhao
 * @create 2018/5/30
 */
@Configuration

@EnableFeignClients(basePackages = {EnableFanWxSDK.PACKAGE})
public class WxSDKConfig {

	// 解决微信返回参数为[text/plain] 无法转化为json
	@Bean
	public Decoder feignDecoder(){
		WxMessageConverter wxConverter = new WxMessageConverter();
		ObjectFactory<HttpMessageConverters> objectFactory = () -> new HttpMessageConverters(wxConverter);
		return new SpringDecoder(objectFactory);
	}




}
