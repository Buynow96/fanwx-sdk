package cn.zynworld.wwjcloud.wwjcloud.wxsdk.sdk;

import cn.zynworld.wwjcloud.wwjcloud.wxsdk.dto.WxAccessTokenDTO;
import cn.zynworld.wwjcloud.wwjcloud.wxsdk.dto.WxJsApiTicketDTO;
import cn.zynworld.wwjcloud.wwjcloud.wxsdk.dto.WxJsSignatureDTO;
import cn.zynworld.wwjcloud.wwjcloud.wxsdk.dto.WxWebAccessTokenDTO;
import cn.zynworld.wwjcloud.wwjcloud.wxsdk.feign.WxAuthFeign;
import cn.zynworld.wwjcloud.wwjcloud.wxsdk.manager.WxInfoManager;
import cn.zynworld.wwjcloud.wwjcloud.wxsdk.utlis.WxSignUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @auther Buynow Zhao
 * @create 2018/5/29
 * 封装简化微信认证相关操作
 */
@Component
public class WxAuthSDK {

	private Logger logger = LoggerFactory.getLogger(WxAuthSDK.class);

	@Autowired
	private WxInfoManager wxInfoManager;
	@Autowired
	private WxAuthFeign wxAuthFeign;
	//负责定时任务
	private final Timer timer = new Timer();

	// 调用微信api的凭证
	private WxAccessTokenDTO wxAccessTokenDTO;
	// 公众号用于调用微信JS接口的临时票据
	private WxJsApiTicketDTO wxJsApiTicketDTO;

	@PostConstruct
	public void init() {
		logger.info("创建定时任务: 定时刷新获取新的微信AccessToken凭证");
		// 定时刷新每小时刷新一次 获取新的accessToken
		// TODO 后续引入定时任务框架
		timer.schedule(new TimerTask() {
			public void run() {
				createWxAccessToken();
				createWxJsApiTicket();
			}
		}, 0, 1000 * 60 * 60);
	}

	/**
	 * @return 返回当前的字符串 微信凭证
	 */
	public String getWxAccessTokenString() {
		return this.wxAccessTokenDTO.getAccess_token();
	}
	/**
	 * @return 返回当前的字符串 微信凭证
	 */
	public String getWxJsApiTicketString() {
		return this.wxJsApiTicketDTO.getTicket();
	}
	/**
	 * @return 返回当前的微信凭证对象 WxAccessTokenDTO
	 */
	public WxAccessTokenDTO getWxAccessTokenDTO() {
		return wxAccessTokenDTO;
	}

	public WxJsApiTicketDTO getWxJsApiTicketDTO() {
		return wxJsApiTicketDTO;
	}

	/**
	 * 通过微信回调的网页授权认证code值获取网页授权凭证{@link WxWebAccessTokenDTO}
	 * @param code 微信回调的网页授权认证code
	 * @return 网页授权凭证{@link WxWebAccessTokenDTO}
	 */
	public WxWebAccessTokenDTO getWxWebAccessTokenByCode(String code) {
		return wxAuthFeign.getWebAccessToken(wxInfoManager.getAppid(), wxInfoManager.getAppsecret(), code, "authorization_code");
	}

	/**
	 * 通过refreshToken来刷新网页授权凭证
	 */
	public WxWebAccessTokenDTO refreshWxWebAccessTokenByRefreshToken(String refreshToken) {
		return wxAuthFeign.refreshWebAccessToken(wxInfoManager.getAppid(), refreshToken, "refresh_token");
	}



	/**
	 * 验证签名 确认接入微信服务器
	 * @return 验证成功true
	 */
	public boolean checkSignature(String signature,String timestamp,String nonce) {
		return WxSignUtil.checkSignature(wxInfoManager.getToken(), signature, timestamp, nonce);
	}

	/**
	 * 为js api 创建签名
	 * @param url 当前网页的URL，不包含#及其后面部分
	 * @return 签名及其附属信息
	 */
	public WxJsSignatureDTO createWxJsSignature(String url) {
		String jsApiTicketString = this.getWxJsApiTicketString();
		WxJsSignatureDTO wxJsSignatureDTO = new WxJsSignatureDTO();
		wxJsSignatureDTO.setAppId(wxInfoManager.getAppid());
		wxJsSignatureDTO.setNonceStr(WxSignUtil.getRandomString());
		wxJsSignatureDTO.setTimestamp(WxSignUtil.getTimestamp());
		String signature = WxSignUtil.createJsApiSignature(
				jsApiTicketString, url,
				wxJsSignatureDTO.getNonceStr(),
				wxJsSignatureDTO.getTimestamp());
		wxJsSignatureDTO.setSignature(signature);
		return wxJsSignatureDTO;
	}


	// 从微信api获取新的WxAccessToken
	private WxAccessTokenDTO createWxAccessToken() {
		WxAccessTokenDTO wxAccessTokenDTO = wxAuthFeign.getAccessToken("client_credential", wxInfoManager.getAppid(), wxInfoManager.getAppsecret());
		this.wxAccessTokenDTO = wxAccessTokenDTO;
		return wxAccessTokenDTO;
	}

	// 从微信获取新的js凭证
	private WxJsApiTicketDTO createWxJsApiTicket() {
		this.wxJsApiTicketDTO = wxAuthFeign.getWxJsApiTicket(this.getWxAccessTokenString(), "jsapi");
		return this.wxJsApiTicketDTO;
	}




}
