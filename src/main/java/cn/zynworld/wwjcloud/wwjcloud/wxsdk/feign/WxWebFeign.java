package cn.zynworld.wwjcloud.wwjcloud.wxsdk.feign;

import cn.zynworld.wwjcloud.wwjcloud.wxsdk.dto.WxWebAccessTokenDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 微信公众号 web 网页相关操作
 */
@FeignClient(url = "${wx.sdk.url}",name = "WxWebSDK")
public interface WxWebFeign {



}
