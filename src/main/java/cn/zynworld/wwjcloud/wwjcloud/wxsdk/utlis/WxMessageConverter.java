package cn.zynworld.wwjcloud.wwjcloud.wxsdk.utlis;

import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * @auther Buynow Zhao
 * @create 2018/5/30
 * 创建一个微信解码器 解决微信响应为 text/plain 无法转json bug
 */
public class WxMessageConverter extends MappingJackson2HttpMessageConverter {
	public WxMessageConverter(){
		List<MediaType> mediaTypes = new ArrayList<>();
		mediaTypes.add(MediaType.TEXT_PLAIN);
		setSupportedMediaTypes(mediaTypes);
	}
}