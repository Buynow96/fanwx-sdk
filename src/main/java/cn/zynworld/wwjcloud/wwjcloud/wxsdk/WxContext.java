package cn.zynworld.wwjcloud.wwjcloud.wxsdk;

import cn.zynworld.wwjcloud.wwjcloud.wxsdk.manager.WxInfoManager;
import cn.zynworld.wwjcloud.wwjcloud.wxsdk.sdk.WxAuthSDK;
import cn.zynworld.wwjcloud.wwjcloud.wxsdk.sdk.WxUserSDK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @auther Buynow Zhao
 * @create 2018/5/31
 * 整合所有sdk 用户只需注入该对象即可
 */
@Component
public class WxContext {

	@Autowired
	private WxInfoManager wxInfoManager;
	@Autowired
	private WxAuthSDK wxAuthSDK;
	@Autowired
	private WxUserSDK wxUserSDK;

	/**
	 * 存储了微信相关信息
	 */
	public WxInfoManager info() {
		return wxInfoManager;
	}

	/**
	 * 微信认证相关，维护各类凭证
	 */
	public WxAuthSDK auth() {
		return wxAuthSDK;
	}

	/**
	 * 微信用户相关
	 */
	public WxUserSDK user() {
		return wxUserSDK;
	}
}
