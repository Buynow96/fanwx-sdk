# wwjcloud-wx-sdk

> 这是一个为娃娃机项目微信部分提供微信开发基础支持的模块
，随着功能的逐步完善后续将独立出来作为我的个人微信开发框架。

# 特点
- 模块基于Spring boot 2.0.2.RELEASE
- 自动维护AccessToken无需手动刷新

# Quick Start

## 配置
### Spring boot 配置
只需要配置你的Spring boot Application上的@SpringBootApplication注解
```
// 添加包扫描路径 EnableFanWxSDK.class 同时需要注意不要遗漏当前项目的扫描路径包 如MyApplication.class
@SpringBootApplication(scanBasePackageClasses = {MyApplication.class,EnableFanWxSDK.class})
public class MyApplication {...}
```
### application.properties 配置项
需要在你的application.properties中配置以下微信信息
- wx.appid=微信appid
- wx.appsecret=微信secret
- wx.token=微信token
- wx.cn.zynworld.wwjcloud.wwjcloud.wxsdk.sdk.url=https://sz.api.weixin.qq.com

## 接入
### 微信接入
```
// 通过Autowired引入SDK Context 对象
@Autowired
private WxContext wx;

// 接入微信服务器
@GetMapping(path = "wx")
public String wx(@RequestParam String signature ,@RequestParam String timestamp ,@RequestParam String nonce,@RequestParam String echostr) {
    boolean checkSuccess = wx.auth().checkSignature( signature, timestamp, nonce);
    if (checkSuccess) {
        return echostr;
    }
    return "check error";
}
```

### 使用SDK
通过下面的方式直接注入即可
```
@Autowired
private WxContext wx;
```
在之后的SDK调用说明中间忽略SDK的注入

## 认证SDK - WxAuthSDK

### 获取当前微信AccessToken
在wxAuthSDK自主维护其AccessToken，并将在有效期内自动更新
```
// 返回的是直接的AccessToken字符串
wx.auth().getWxAccessTokenString();

// 返回的是被封装的WxAccessTokenDTO 对象
wx.auth().getWxAccessTokenDTO()
```

### 网页授权认证
```
// 将code 转为网页授权认证对象 WxWebAccessTokenDTO
wx.auth().getWxWebAccessTokenByCode(code)

// 通过该方法在需要时可以刷新过期的认证
wx.auth().refreshWxWebAccessTokenByRefreshToken(refreshToken)
```

### 为网页js api 创建凭证
```
@GetMapping(path = "wx/jssignature")
// WxJsSignatureDTO 对象封装了 jssdk 所需要的签名及其附属必要信息
public WxJsSignatureDTO createJsSignature(@RequestParam("url") String url) {
    WxJsSignatureDTO wxJsSignature = wx.auth().createWxJsSignature(url);
    return wxJsSignature;
}
```

## 用户SDK - WxUserSDK
```
// 通过openid 获取用户信息
// 返回的WxUserBaseInfoDTO 封装了用户的基本信息
wx.user().getUserInfoByOpenid(openid)
```